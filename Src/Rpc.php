<?php

namespace W7;

use W7\Transport\ClientAbstract;
use W7\Transport\Http\Client as HttpClient;
use W7\Transport\Tcp\Protocol\Thrift\Client as ThriftClient;
use W7\Transport\Tcp\Protocol\Json\Client as JsonClient;

class Rpc {
	private $options;
	private $client;

	private $protocolMap = [
		'tcp' => JsonClient::class,
		'tcp-thrift' => ThriftClient::class,
		'ssl' => JsonClient::class,
		'ssl-thrift' => ThriftClient::class,
		'http' => HttpClient::class,
		'https' => HttpClient::class
	];

	/**
	 * Rpc constructor.
	 * @param array $options
	 * [
	 *      'base_uri' => '',
	 *      'cert' => null,
	 *
	 *      自定义参数
	 *      'send_timeout' => 0,
	 *      'recv_timeout' => 0,
	 *      'thrift_protocol' => '',
	 *      'thrift_transport' => ''
	 * ]
	 */
	public function __construct(array $options = [
		'base_uri' => ''
	]) {
		$this->options = $options;
		$this->options['pack_format'] = $options['pack_format'] ?? 'json';
		$this->setBaseUrl($options['base_uri'] ?? '');
	}

	/**
	 * @param $url
	 * @return $this
	 * tcp://127.0.0.1:8080
	 * https://127.0.0.1:8080
	 * http://127.0.0.1:8080
	 */
	public function setBaseUrl($url) {
		$this->options['base_uri'] = $url;

		$urlInfo = parse_url($url);
		$this->options['host'] = $urlInfo['host'] ?? '';
		$this->options['port'] = $urlInfo['port'] ?? '';
		$this->options['protocol'] = $urlInfo['scheme'] ?? '';
		if (!in_array($this->options['protocol'], array_keys($this->protocolMap))) {
			throw new \Exception('not support this scheme ' . $this->options['protocol'], 500);
		}

		$this->client = null;
		return $this;
	}

	private function getClient() : ClientAbstract {
		if (!$this->client) {
			$class = $this->protocolMap[$this->options['protocol']];
			$this->client = new $class($this->options);
		}

		return $this->client;
	}

	public function getResponse() {
		return $this->client->response;
	}

	public function __call($name, $arguments) {
		if (!in_array($name, ['post', 'get', 'put', 'delete', 'options', 'patch', 'head'])) {
			throw new \Exception('not support request method ' . $name);
		}

		if (count($arguments) == 1) {
			$arguments[] = [];
		}

		try{
			$name = strtoupper($name);
			$client = $this->getClient();
			$ret = $client->call($name, ...$arguments);
		} catch (\Throwable $e) {
			$ret = [
				'error' => $e->getMessage()
			];
		}

		return $ret;
	}
}