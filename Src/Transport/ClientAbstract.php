<?php

namespace W7\Transport;

use Psr\Http\Message\ResponseInterface;

abstract class ClientAbstract {
	protected $options;
	protected $packFormat;
	public $response;

	public function __construct(array $options) {
		$this->options = $options;
		$this->packFormat = $options['pack_format'];
		unset($this->options['pack_format']);
		$this->init();
	}

	abstract protected function init();
	abstract protected function request($method, $url, $params) : ResponseInterface;

	public function pack( $body) {
		switch ($this->packFormat) {
			case 'serialize':
				return serialize($body);
			case 'json':
				return json_encode($body);
			default:
				return $body;
		}
	}

	public function unpack($body) {
		switch ($this->packFormat) {
			case 'serialize':
				return unserialize($body);
			case 'json':
				return json_decode($body, true);
			default:
				return $body;
		}
	}

	public function call($method, $url, $params) {
		$response = $this->request($method, $url, $params);
		$this->response = $response;

		return $this->unpack($this->response->getBody()->getContents());
	}
}