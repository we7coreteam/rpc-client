<?php

namespace W7\Transport\Tcp;

class Socket {
	protected $handle;
	protected $options;


	/**
	 * Socket constructor.
	 * @param array $options
	 */
	public function __construct(
		$options = []
	) {
		$this->options = $options;
		$this->handle = new \swoole_client($this->getSocketType());
		$this->handle->set($options);
	}

	private function getSocketType() {
		$this->options['type'] = SWOOLE_TCP | SWOOLE_KEEP;

		if (strpos($this->options['protocol'], 'udp') !== false) {
			$this->options['type'] |= SWOOLE_UDP;
		}
		if (strpos($this->options['protocol'], 'ssl') !== false) {
			$this->options['type'] |= SWOOLE_SSL;
		}

		return $this->options['type'];
	}

	private function getTimeOut() {
		if (empty($this->options['timeout'])) {
			/**
			 * swoole_client default timeout
			 */
			return 0.5;
		}

		return $this->options['timeout'];
	}

	/**
	 * Connects the socket.
	 */
	public function open() {
		if (empty($this->options['host'])) {
			throw new \Exception('Cannot open null host', 500);
		}
		if ($this->options['port'] <= 0) {
			throw new \Exception('Cannot open without port', 500);
		}

		$this->handle->connect($this->options['host'], $this->options['port'], $this->getTimeOut());

		if ($this->handle->isConnected() == false) {
			$error = 'swoole_client: Could not connect to ' . $this->options['host'] . ':' . $this->options['port'] . ' with error code ' . $this->handle->errCode;
			throw new \Exception($error, 500);
		}
	}

	public function read($size = 65535, $flags = \swoole_client::MSG_WAITALL) {
		$data = $this->handle->recv($size, $flags);
		if ($data === false) {
			throw new \Exception('swoole_client: Could not read  from ' .
				$this->options['host'] . ':' . $this->options['port'] . ' with error code ' . $this->handle->errCode,
				500);
		}

		return $data;
	}

	/**
	 * 兼容thrift socket标准
	 * @param $len
	 * @return string
	 * @throws \Exception
	 */
	public function readAll($len) {
		$data = '';
		$got = 0;
		while (($got = $this->strlen($data)) < $len) {
			$data .= $this->read($len - $got);
		}

		return $data;
	}

	private function strlen($str) {
		if (ini_get('mbstring.func_overload') & 2) {
			return mb_strlen($str, '8bit');
		} else {
			/**
			 * mbstring is not installed or does not have function overloading
			 * of the str* functions enabled so use PHP core str* functions for
			 * byte counting.
			 */
			return strlen($str);
		}
	}

	public function write($buf) {
		$ret = $this->handle->send($buf);
		if ($ret === false) {
			throw new \Exception(
				'swoole_client: Could not write ' . $buf . ' bytes ' .
				$this->options['host'] . ':' . $this->options['port'] . ' with error code ' . $this->handle->errCode,
			500);
		}
	}

	/**
	 * 兼容thrift socket标准
	 */
	public function flush() {
		// no-op
	}

	public function close() {
		$this->handle->close();
	}
}