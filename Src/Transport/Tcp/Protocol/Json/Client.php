<?php

namespace W7\Transport\Tcp\Protocol\Json;

use W7\Http\Message\Server\Response;
use Psr\Http\Message\ResponseInterface;
use W7\Transport\ClientAbstract;
use W7\Transport\Tcp\Socket;

class Client extends ClientAbstract {
	private $socketHandle;

	protected function init() {
		$this->socketHandle = new Socket($this->options);
	}

	public function request($method, $url, $params) : ResponseInterface {
		$this->socketHandle->open();

		$body = [
			'url' => $url
		];
		if ($params) {
			$body['data'] = $params;
		}
		$body = $this->pack($body);

		$this->socketHandle->write($body);
		$ret = $this->socketHandle->read(65535, 0);
		$this->socketHandle->close();

		$response = new Response();
		return $response->withContent($ret);
	}
}