<?php

namespace W7\Transport\Tcp\Protocol\Thrift;

use Thrift\Protocol\TMultiplexedProtocol;
use Psr\Http\Message\ResponseInterface;
use W7\Http\Message\Server\Response;
use W7\Transport\ClientAbstract;
use W7\Transport\Tcp\Protocol\Thrift\Core\DispatcherClient;
use W7\Transport\Tcp\Socket;

class Client extends ClientAbstract {
	private $socketHandle;
	private $tProtocol;
	private $tTransport;
	private $dispatcher;

	protected function init() {
		$this->initSocket();
		$this->initTransport();
		$this->initProtocol();
		$this->initDispatcher();
	}

	private function initSocket() {
		$this->socketHandle = new Socket($this->options);
	}

	private function initProtocol() {
		$protocol = $this->options['thrift_protocol'] ?? 'TBinaryProtocol';
		$protocol = "\\Thrift\Protocol\\" . $protocol;

		$this->tProtocol = new $protocol($this->tTransport);
	}

	private function initTransport() {
		$transport = $this->options['thrift_transport'] ?? 'TFramedTransport';
		$transport =  "\\Thrift\Transport\\" . $transport;

		$this->tTransport = new $transport($this->socketHandle);
	}

	private function initDispatcher() {
		$service = new TMultiplexedProtocol($this->tProtocol, 'Dispatcher');
		$this->dispatcher = new DispatcherClient($service);
	}

	public function request($method, $url, $params) : ResponseInterface {
		$this->tTransport->open();

		$body = [
			'url' => $url
		];
		if ($params) {
			$body['data'] = $params;
		}
		$body = $this->pack($body);

		$ret = $this->dispatcher->run($body);
		$this->tTransport->close();

		$response = new Response();
		return $response->withContent($ret);
	}
}