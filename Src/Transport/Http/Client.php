<?php

namespace W7\Transport\Http;

use W7\Transport\ClientAbstract;
use GuzzleHttp\Client as HttpClient;
use Psr\Http\Message\ResponseInterface;

class Client extends ClientAbstract {
	private $handle;

	protected function init() {
		$this->handle = new HttpClient($this->options);
	}

	/**http
	 * @param $url
	 * @param null $params
	 * @return mixed
	 */
	public function request($method, $url, $params) : ResponseInterface {
		return $this->handle->request($method, $url, $params);
	}
}